Working with Linux File/Directory permission & owernship:
=========================================================
[root@node1 ~]# mkdir linux21
[root@node1 ~]# cd linux21
[root@node1 linux21]# ls
[root@node1 linux21]# mkdir day8
[root@node1 linux21]# cd day8

[root@node1 day8]# touch file1              ; বর্তমান ডিরেক্টরি 'day8' এর মধ্যে 'file1' নামে একটা ফাইল তৈরি করা হল। 
[root@node1 day8]# mkdir dir1               ; বর্তমান ডিরেক্টরি 'day8' এর মধ্যে 'dir1' নামে একটা ডিরেক্টরি তৈরি করা হল। 
[root@node1 day8]# cp /etc/passwd  .        ; '/etc/passwd' ফাইলটি কপি করে বর্তমান ডিরেক্টরি 'day8' মধ্যে নিয়ে আসা হয়েছে। 
[root@node1 day8]# ll

d rwxr-xr-x.  2  root  root     6  May 10 17:54  dir1
- rw-r--r--.  1  root  root  2662  May 10 17:55  passwd
- rw-r--r--.  1  root  root     0  May 10 17:54  file1
 
1      2      3   4     5       6          7       8

 1: file/dir types (লিনাক্সে মোট সাত ধরনের (-, d, c, b, l, s, p ) ফাইল/ডিরেক্টরি পাওয়া যাবে) 
 2: file/dir permission (User + Group + Others + ACL)
 3: file/dir link (Hard Link) - ফাইলের ক্ষেত্রে '1' এবং ডিরেক্টরির ক্ষেত্রে '2' থাকবে, তবে ভিতরে যত ডিরেক্টরি থাকবে তার উপরে ভিত্তি করে সংখ্যা বাড়বে। 
 4: file/dir owner (যে ইউজার ফাইল/ডিরেক্টরিটি তৈরি করেছে তার নাম থাকবে) 
 5: file/dir group owner - (ইউজার যে গ্রুপের সদস্য সেই গ্রুপের নাম থাকবে) 
 6: file/dir size (byte) - ফাইল হলে '0' বাইট, এবং ডিরেক্টরি হলে '6' বাইট। 
 7: file/dir modify date
 8: file/dir name

 Link/Mp3 - Cyan
 image/video - Magenta
 zip/rar/rpm - red
 text - b&w
 green - executable
 yellow - Device
 blue - directory 

######################
###  Session 01    ###
######################

Working with link file:
=======================
লিঙ্ক ফাইলের (l) অর্থ, একটি ফাইলের সাথে আরেকটি ফাইলের লিঙ্ক থাকে। অর্থাৎ কোনো ফাইলে কোনো লেখা/টেক্সট মোডিফাই/আপডেট করলে অপর ফাইলটিও একই ভাবে আপডেট হবে। যেমনঃ উইন্ডোজ অপারেটিং সিস্টেমের ক্ষেত্রে শর্টকাট ফাইল। ফাইলের শর্টকাট গুলা থাকে আমাদের ডেক্সটপে এবং সেগুলার সোর্স (Origin) থাকে অন্য লোকেশনে। আমরা যদি ডেক্সটপ থেকে সেই সকল শর্টকাট ফাইলের উপরে ক্লিক করি, তাহলে মূল সোর্স (Origin) থেকে রান করবে।

 লিনাক্স সিস্টেমে দুই ধরণের লিঙ্ক ফাইল আছেঃ  

Type of link file:
------------------
 1) Hard link - ফাইল গুলার আইনোড নাম্বার (inode) একই হবে। অর্থাৎ দুইটি ফাইলের মাঝে যদি হার্ডলিঙ্ক করা হয়, তাদের কনটেন্ট, সাইজ, পার্মিশন একই হবে, শুধু নাম আলাদা হবে। এক্ষেত্রে মূল ফাইলটি ডিলিট হলেও লিংক ফাইলের কোনো পরিবর্তন বা সমস্যা হবে না। হার্ডলিঙ্ক শুধুমাত্র ফাইলের ক্ষেত্রেই করা যাবে। 
 
 2) Soft link - ফাইল গুলার আইনোড নাম্বার (inode) ভিন্ন হবে। দুইটা ফাইল/ডিরেক্টরির মধ্যে যদি সফট লিঙ্ক করা হয়, তাদের কনটেন্ট একই হবে শুধু নাম আলাদা হবে। সফটলিঙ্ক বা symbolik লিঙ্ক ফাইলের শুরুতে 'l' থাকে। এক্ষেত্রে যদি মূল ফাইলটি ডিলিট করা হয়, তাহলে লিংক ফাইল কোনো কাজ করবে না। সফট লিঙ্ক ফাইল এবং ডিরেক্টরি উভয়ের ক্ষেত্রেই করা যায়। 

[root@node1 day8]# ls -li 
[root@node1 day8]# tail passwd
[root@node1 day8]# ln -s  passwd passwd-soft        ;softlink তৈরির ক্ষেত্রে '-s' ব্যবহার করতে হবে। 
[root@node1 day8]# ln passwd passwd-hard	      ;hardlink তৈরির কমান্ড। 
[root@node1 day8]# ll
[root@node1 day8]# tail passwd-hard
[root@node1 day8]# tail passwd-soft
[root@node1 day8]# ls -li

[root@node1 day8]# echo "The End" >> passwd
[root@node1 day8]# tail -3 passwd
[root@node1 day8]# tail -3 passwd-hard
[root@node1 day8]# tail -3 passwd-soft
[root@node1 day8]# rm -f passwd
[root@node1 day8]# ll
[root@node1 day8]# tail -3 passwd-hard
[root@node1 day8]# tail passwd-soft
[root@node1 day8]# ln -s /home/student/Documents studentdir

######################
###  Session 02    ###
######################

 Field no: 2, 4 & 5 (Permission) 
 ------------------------------- 
 [root@node1 day8]#  ll 

 - rw-r--r--. 1  root  root    0   Sep 26 09:52  file1
 d rwxr-xr-x. 2  root  root  4096  Sep 26 09:33  dir1
      (2)        (4)   (5)
 
 subfield:
 ---------
  - rw-  r--  r--  .  = file1  - ডিফল্ট ভাবে লিনাক্সের ফাইলের কোনো এক্সিকিউট (x) পার্মিশন থাকে না। 
  d rwx  r-x  r-x  .  = dir1   - ডিফল্ট ভাবে লিনাক্সের ডিরেক্টরিতে ইউজার, গ্রুপ এবং অন্যান্য ইউজারদের জন্য এক্সেকিউট (x) পার্মিশন থাকে। 
    (u)  (g)  (o) (A)  

 u = user 
 g = group
 o = others
 A = ACL Permission (.) 

 r = read (4)     - ফাইলের ক্ষেত্রে 'read' অর্থ ফাইলটি পড়তে পারবে এবং ডিরেক্টরির ক্ষেত্রে read অর্থ, ডিরেক্টরি টা কপি করা যাবে। 
 w = write (2)    - ফাইলের ক্ষেত্রে 'write' অর্থ ফাইলটি এডিট এবং সেভ করা যাবে। ডিরেক্টরির ক্ষেত্রে write অর্থ, ডিরেক্টরির ভিতরে paste                                                            করা যাবে। 

 x = execute (1)  - ফাইলের ক্ষেত্রে 'execute' পার্মিশন দরকার নাই। কিন্ত, ফাইলটি যদি, প্রোগাম/স্ক্রিপ্ট/এপ্লিকেশন হয়, তাহলে অবশ্যই execute                                                       পার্মিশন থাকতে হবে। ডিরেক্টরির ক্ষেত্রে 'execute' হচ্ছে ডিরেক্টরির মধ্যে প্রবেশ করার জন্য, অর্থাৎ যে ডিরেক্টরিতে                                       		   	   'execute' পার্মিশন নাই, সেখানে প্রবেশ করা যাবে না।   

 - = no permission (0)  - এক্ষেত্রে 'read/write/execute' কোনো পার্মিশন থাকছে না। 

 . = ACL Permission (+) - ফাইল/ডিরেক্টরি উভয়ের ক্ষেত্রে এখানে  dot '(.)' থাকবে। কিন্ত, যখন ACL (File Access Control) এপ্লাই করা 			   		    হবে তখন, এখানে Dot (.) এর পরিবর্তে '+' সাইন চলে আসবে। 

 - rw- r-- r-- .  = file1  (644)
 d rwx r-x r-x .  = dir1   (755) 

   rw- = 4 + 2 + 0 = 6 (U)     rwx = 4 + 2 + 1 = 7 (U)
   r-- = 4 + 0 + 0 = 4 (G)     r-x = 4 + 0 + 1 = 5 (G)
   r-- = 4 + 0 + 0 = 4 (O)     r-x = 4 + 0 + 1 = 5 (O) 
   -------------------        --------------------
 		  644			  = 755

 ** ডিফল্ট ফাইল পার্মিশন = 644  এবং ডিরেক্টরি পার্মিশন = 755 তবে, আমরা আমাদের প্রয়োজনে পার্মিশন লেভেল পরিবর্তন করতে পারি। 

 Maximum File Permission: 666       =>  (rw- rw- rw-)
 Maximum Directory Permission: 777  =>  (rwx rwx rwx)

 Class Work:
 ----------
 740 = rwx r-- --- 
 735 = 
 642 = 
 503 = 
 150 = 
 326 = 
 467 = 
 631 = 

Lab Demo: Linux Standard File Permission
----------------------------------------
 এখানে 'file1' নিয়ে কাজ করা হবে। যেখানে একটা গ্রুপ থাকবে 'students' নামে এবং 'students' গ্রুপের মেম্বার হবে 'sadat, sumon, malek'। সেই সাথে একজন গেস্ট ইউজার (Others) থাকবে 'tamim' নামে।  

 Group:      Members                 others
 ======      =======                 ======
 students:   malek, sumon, sadat     all (except group members)

[root@node1 day8]# groupadd students
[root@node1 day8]# useradd -G students malek
[root@node1 day8]# useradd -G students sadat
[root@node1 day8]# useradd -G students sumon

[root@node1 day8]# useradd tamim

[root@node1 day8]# grep students /etc/group
students:x:5005:malek,sadat,sumon

এখন 'file1' কে 'Malek' ইউজারের Ownership এবং 'students' গ্রুপের Group Ownership দেওয়া হবে। 'malek' ইউজারের জন্য পূর্ণ (rwx) পার্মিশন দেওয়া হবে। পাশাপাশি 'students' গ্রুপের ইউজারদের শুধু read (r) পার্মিশন থাকবে এবং অন্য ইউজারের/গেস্ট  (others) ক্ষেত্রে কোনো  প্রকার পার্মিশন থাকবে না। 

 user: (malek) : full permission (rwx)
 group: students: read (r--)
 others: others : no   (---)

[root@node1 day8]# ls -l
 -rw-r--r--. 1 root root 0 Jun 14 19:55 file1

[root@node1 day8]# chown malek file1                ; chown কমান্ড ব্যবহার করে Ownership পরিবর্তন করা যায়। 
[root@node1 day8]# ls -l
-rw-r--r--.  1 malek root 0 Jun 14 19:55 file1

[root@node1 day8]# chgrp students file1            ; chgrp কমান্ড ব্যবহার করে Group Ownership পরিবর্তন করা যায়। 
[root@node1 day8]# ls -l 
-rw-r--r--. 1 malek students 0 Sep 26 09:52 file1

[root@node1 day8]# chmod 740 file1                  ; chmod কমান্ড ব্যবহার করে পার্মিশন (rwx) পরিবর্তন করা যায়। 
[root@node1 day8]# ls -l
-rwxr-----. 1 malek students 0 Jun 14 19:55 file1

 Testing:
 --------
 (Ownership Test)
[root@node1 day8]# su malek
[malek@node1 day8]$ ls
[malek@node1 day8]$ echo "This is malek" > file1      ; malek can rw
[malek@node1 day8]$ cat file1
[malek@node1 day8]$ exit

 (Groupowner Test)
[root@node1 day8]# su sumon
[sumon@node1 day8]$ ls
[sumon@node1 day8]$ cat file1
[sumon@node1 day8]$ echo "This is sumon" > file1       ; Permission Denied (only read)
[sumon@node1 day8]$ exit

 (Others)

[root@node1X day8]# su tamim
[tamim@node1X day8]$ cat file1   ; access denied     ;  Permission Denied (no permission)
[tamim@node1X day8]$ echo "This is tamim" > file1     ;  Permission Denied (no permission)
[tamim@node1X day8]$ exit

Linux File Access Control List (ACL):
=====================================
***  যদি একটা ফাইল/ডিরেক্টরিতে একাধিক Ownership বা Group Ownership দরকার হয়। 
***  একটা ফাইল/ডিরেক্টরিতে আলাদা আলদা ইউজার এবং গ্রুপের আলাদা আলাদা পার্মিশন দরকার হলে। 
***  নির্দিষ্ট কোনো গ্রুপ থেকে নির্দিষ্ট কোনো ইউজারের প্রিভিলেজ তুলে নিতে চাইলে বা পরিবর্তন করতে চাইলে। 
***  আমরা যদি গেস্ট  (others) ইউজার থেকে কাউকে স্পেশাল প্রিভেলেজ দিতে চাই। 

তাহলে আমরা যদি উপরে উল্লেখিত ব্যাপারে চিন্তা করি, তাহলে আমাদের স্ট্যান্ডার্ড ফাইল/ডিরেক্টরি পার্মিশন ব্যবহার করে সেটা করা সম্ভব না। সেক্ষেত্রে ব্যবহার করতে হবে 'ACL' পার্মিশন। ACL পূর্ণ অর্থ (Access Control List), যেটা আমরা ফাইল এবং ডিরেক্টরি উভয়ের ক্ষেত্রে ব্যবহার করতে পারি। ACL সিকিউরিটি সার্ভিস লিনাক্স ফাইল সিস্টেমের (ext4/xfs) উপরে নির্ভর করে। যেহেতু আমাদের ফাইল সিস্টেম 'xfs' সুতরাং এটা ডিফল্ট ভাবে এনাবেল থাকে। 
 
Working Directory:
------------------
[root@node1X day8]# ls 
[root@node1X day8]# touch tutorial profile
[root@node1X day8]# ll

-rw-r--r--. 1 root root 0 Jun 28 17:00 tutorial
-rw-r--r--. 1 root root 0 Jun 28 17:00 profile

এই ল্যাব ডেমো প্র্যাকটিস করার জন্য 'jack', 'rose', 'tomy' নামে তিন জন ইউজার এবং 'support' নামে গ্রুপ একাউন্ট তৈরি করা হয়েছে। 
 
 [root@node1X day8]# useradd jack
 [root@node1X day8]# useradd rose
 [root@node1X day8]# useradd tomy
 [root@node1X day8]# groupadd support

ACL Test:
----------
[root@node1X day8]# getfacl profile      ; ফাইল/ডিরেক্টরিতে ACL পার্মিশন দেখার কমান্ড। 
 # owner: root
 # group: root 
 user::rw-
 group::r--
 other::r--

 ACL Command Options:
 --------------------
 - m = modify
 - x = remove
 - b = blank 
 - R = Recrusivly

User ACL:
---------
Lab: আমরা নিচের উদাহরণে, 'profile' ফাইলে jack এর জন্য 'rwx', ইউজার 'rose' এর জন্য 'r--' এবং ইউজার 'tomy' এর ক্ষেত্রে কোনো পার্মিশন দেওয়া হবে না। 

[root@serverX day8]# setfacl -m u:jack:rwx,rose:r--,tomy:---  profile     ;ফাইল/ডিরেক্টরিতে ACL পার্মিশন দেওয়ার কমান্ড। 

[root@serverX day8]# ll
-rw-rw-r--+ 1 root root 0 Jun 28 17:00 profile

[root@serverX day8]# getfacl profile 
# file: profile
# owner: root
# group: root
user::rw-
user:jack:rwx
user:rose:r--
user:tomy:---
group::r--
mask::rwx
other::r--

উপরের কমান্ড আউটপুটে দেখা যাচ্ছে, ইউজার 'jack' এর ক্ষেত্রে 'rwx' যোগ হয়েছে। পাশাপাশি ইউজার 'rose' এর ক্ষেত্রে 'r' এবং other ইউজার 'tomy' এর ক্ষেত্রে পার্মিশন তুলে নেওয়া হয়েছে। 

[root@serverX day8]# su jack
[jack@serverX day8]$ cat profile
[jack@serverX day8]$ echo this is Jack > profile
[jack@serverX day8]$ cat profile
[jack@serverX day8]$ exit

[root@serverX day8]# su rose
[rose@serverX day8]$ cat profile
[rose@serverX day8]$ echo this is Rose >> profile           ; permission denied
[rose@serverX day8]$ exit

[root@serverX day8]# su tomy
[tomy@serverX day8]$ cat profile
[tomy@serverX day8]$ echo this is Tomy >> profile
[tomy@serverX day8]$ exit

***** Permission Denied

Group acl:
----------
[root@serverX day8]# setfacl -m g:support:rw-  tutorial 
[root@serverX day8]# getfacl tutorial

Directory Permission:
---------------------
[root@serverX day8]# mkdir acldir
[root@serverX day8]# touch acldir/file1
[root@serverX day8]# touch acldir/file2
[root@serverX day8]# ls -l acldir
[root@serverX day8]# setfacl -R -m u:rose:rwx acldir    ;-R (recursively)
[root@serverX day8]# ls -l acldir
[root@serverX day8]# getfacl acldir

ACL Remove (user):
-------------------
[root@serverX day8]# setfacl -x u:rose: profile
[root@serverX day8]# getfacl profile

ACL Remove (Gruop):
------------------
[root@serverX day8]# setfacl -x g:support: tutorial
[root@serverX day8]# getfacl tutorial 

Remove ACL from File:
---------------------
[root@serverX day8]# setfacl -b tutorial 

#################### Sesssion 03 ##########################

Linux SUID, SGID and Sticky Bit Concept:
----------------------------------------
 লিনাক্সের স্ট্যান্ডার্ড পার্মিশনের পাশাপাশি কিছু স্পেশাল পার্মিশন ব্যবহার করা হয়। 

  *** স্ট্যান্ডার্ড পার্মিশন  প্রকাশ করা হয় r=read, w=write, x=execute, -= no permission

  *** স্পেশাল পার্মিশন  প্রকাশ করা হয় ' S,S,T/s,s,t' দিয়ে। 
 
 Read=4 (r)
 Write=2 (w)
 Execute=1 (x)
 no permission = 0 (-)
-------------------
 rwx = 4+2+1 =  7

 উপরের 'Standard Permission' এর সাথে যোগ হবে লিনাক্স স্পেশাল পার্মিশন।  

 4 = SUID  (s,S)  	- SUID পার্মিশন ব্যবহার হয় Owner ফিল্ডে (1st field) এবং এটা প্রকাশ করা হয় 'S' অথবা 's' দিয়ে।  
 2 = SGID  (s,S) 	- SGID পার্মিশন ব্যবহার হয় Group ফিল্ডে (2nd field) এবং এটাও প্রকাশ করা হয় 'S' অথবা 's' দিয়ে।  
 1 = Sticky bit (t, T)  - Sticky পার্মিশন ব্যবহার হয় Other ফিল্ডে (3rd field) এবং এটা প্রকাশ করা হয় 'T' অথবা 't' দিয়ে।  

নোটঃ যদি ফাইল/ডিরেক্টরিতে আগে থেকেই এক্সিকিউট  পার্মিশন থাকে, তাহলে স্পেশাল পার্মিশন সেট করার পরে ছোট হাতের 's,s,t' থাকবে, আর যদি কোনো এক্সিকিউট পার্মিশন (x) না থাকে সেক্ষেত্রে 'S,S,T' 

উদাহরণ স্বরুপঃ 
-----------
 rwx r-x r-x = rws r-s r-t  - 'x' বিট সহ স্পেশাল পার্মিশনের ব্যবহার করা হয়। 
 rw- r-- r-- = rwS r-S r-T  - 'x' বিট বাদে স্পেশাল পার্মিশনের ব্যবহার করা হয়। 
  u   g   o

 x755 (where x = Special Permission for directory)
 x644 (where x = Special Permission for file)

একটা ডিরেক্টরির ক্ষেত্রে যদি চিন্তা করি, 

 4 + 755 = SUID       (1st Field)
 2 + 755 = SGID       (2nd Field)
 1 + 755 = Sticky bit (3rd Field)   

 Working with Test SUID:
 ----------------------
 UID - যখন কোনো ফাইলে/কমান্ডে রেগুলার ইউজার কে সুপার ইউজারের (root) প্রিভেলেজে রান করার দরকার হয়, তখন সেই ফাইল বা কমান্ডের উপরে SUID সেট করা হয়। তার অর্থ উক্ত ফাইল/কমান্ডটি সুপার ইউজারের (root) (file Owner) পাশাপাশি যে কোনো রেগুলার ইউজার একই প্রিভিলেজে এক্সেস/রান করতে পারবে। 

 [root@node1 day8]# which passwd
 [root@node1 day8]# ls -l /bin/passwd
 -rwsr-xr-x. 1 root root 34512 Aug 12  2018 /bin/passwd

 [root@node1 day8]# ls -l /usr/bin/ls
 -rwxr-xr-x. 1 root root 166448 Jan 11  2018 /usr/bin/ls

 [root@node1 day8]# su student
 [student@node1 day8]$ ls /root
  ls: cannot open directory '/root': Permission denied

 [student@node1 day8]$ exit
 [root@node1 day8]# ls -l /usr/bin/ls
 -rwxr-xr-x. 1 root root 166448 Jan 11  2018 /usr/bin/ls

[root@node1 day8]# chmod 4755 /usr/bin/ls       ; SUID  Permission

 [root@node1 day8]# ls -l /usr/bin/ls
 -rwsr-xr-x. 1 root root 166448 Jan 11  2018 /usr/bin/ls
  
 [root@node1 day8]# su student
 [student@node1 day8]$ ls /root
 [student@node1 day8]$ exit

 Class work: Login as 'student' user and view log file '/var/log/messages' using 'tail'
 ----------- 
 Hints: [root@node1 day8]# tail -5 /var/log/messages
 Hints: [student@node1 day8]$ tail -5 /var/log/messages 
 
 Working with SGID:
 ------------------
 SGID যখন ব্যবহার করা হবে, তখন গ্রুপের মেম্বারদের মধ্যে কোনো ইউজার যদি কোনো ফাইল/ডিরেক্টরি তৈরি করলে, তার প্রিভিলেজ আটোমেটিক্যালি গ্রুপের অন্যান্য মেম্বাররা পেয়ে যাবে। অর্থাৎ প্রতিটি ফাইলের গ্রুপ মেম্বারশিপ 'Inherit' হবে। 

  [root@node1 day8]# mkdir resource
  [root@node1 day8]# ls -ld resource 
   drwxr-xr-x. 2 root root 6 Sep 18 22:13 resource

  [root@node1 day8]# grep students /etc/group
   students:x:1008:malek,sumon,sadat
 
 [root@node1 day8]# chmod 770 resource 
 [root@node1 day8]# chgrp students resource 

 [root@node1 day8]# ls -ld resource 
  drwxrwx---. 2 root students 6 Sep 18 22:13 resource

 [root@node1 day8]# su malek
 [malek@node1 day8]$ touch resource/malek1
 [malek@node1 day8]$ exit
 [root@node1 day8]# su sadat
 [sadat@node1 day8]$ touch resource/sadat1
 [sadat@node1 day8]$ exit
 [root@node1 day8]# ll resource 

 [root@node1 day8]# chmod 2770 resource   ; SGID Permission
 [root@node1 day8]# ls -ld resource 
  drwxrws---. 2 root students 6 Sep 18 22:13 resource

 [root@node1 day8]# su sumon
 [sumon@node1 day8]$ touch resource/sumon1
 [root@node1 day8]# ll resource 
 
 Working with Sticky Bit:
 ------------------------
 লিনাক্সে ডিরেক্টরির ম্যাক্সিমাম পার্মিশন হচ্ছে '777' এবং এই ম্যাক্সিমাম পার্মিশন দেওয়ার পরে যে কোনো ইউজার চাইলের এই ডিরেক্টরির মধ্যেকার ফাইল ডিলিট করে দিতে পারে। কিন্ত, আমরা যদি উক্ত ডিরেক্টরির উপরে Sticky bit পার্মিশন প্রয়োগ করি তাহলে, যে কোনো ইউজার ডিরেক্টরির মধ্যে ফাইল তৈরি করতে বা পেস্ট করতে পারবে কিন্ত ডিলিট করতে পারবে না। 

 [root@node1 day8]# ls -ld /tmp

 [root@node1 day8]# mkdir mydir1
 [root@node1 day8]# mkdir mydir2
 
 [root@node1 day8]# ll
 [root@node1 day8]# chmod 777 mydir1     ; standard permission
 [root@node1 day8]# chmod 1777 mydir2    ; sticky bit

 [root@node1 day8]# ls -ld mydir1 mydir2
  drwxrwxrwx. 2 root root 6 Sep 18 21:53 mydir1
  drwxrwxrwt. 2 root root 6 Sep 18 21:53 mydir2

  [root@node1 day8]# touch mydir1/file1
  [root@node1 day8]# touch mydir2/file2
 
 [root@node1 day8]# su student
 [student@node1 day8]$ rm mydir1/file1   ; এটার প্যারেন্ট ডিরেক্টরিতে ফুল পার্মিশন  এটা ডিলিট হয়ে যাবে। 
 [student@node1 day8]$ rm mydir2/file2   ; এটার প্যারেন্ট ডিরেক্টরিতে যেহেতু sticky বিট সেট করা আছে সুতরাং এটা ডিলিট হবে না। 

  rm: cannot remove ‘mydir2/file2’: Operation not permitted



        ------------ Thank you  ----------














