﻿Manage containers:
-------------------
* Find and retrieve container images from a remote registry
* Inspect container images
* Perform container management using commands such as podman and skopeo
* Build a container from a Container file
* Perform basic container management such as running, starting, stopping, and listing running containers
* Run a service inside a container
* Configure a container to start automatically as a systemd service
* Attach persistent storage to a container

Podman একটি কন্টেইনার টুলস যেটা Daemonless (Unlike Docker) পদ্ধতিতে কাজ করে। 

Building a Container Lab Environment:
-------------------------------------
Container সম্পর্কিত ল্যাব ডেমো প্র্যাকটিস করতে হলে আমাদের নিচের লিস্ট অনুযায়ী সিস্টেম রেডি করতে হবেঃ 

=> CPU: 2 Core (64 Bit)  Intel/AMD/ARM
=> Memory: 2 GB
=> Storage: 20 GB 
=> OS: CentOS/RedHat/Rocky
=> IP Address: 172.25.11.254/24

Required Packages for Container:
-------------------------------
* Buildah - কন্টেইনার ইমেজ ( OCI or Docker Format) বিল্ড (build) করার জন্য ব্যবহার করা হয়। 
* Podman  - কন্টেইনার ইমেজ রান করার জন্য ব্যবহার হয়। Podman লোকাল সিস্টমে হতে অথবা রেজিস্ট্রি হতে ইমেজ নামাতে (pull) পারে। 
* Skopeo  - রেজিস্ট্রি থেকে ইমেজ নামানো (Pulling) এবং পাঠানোর (Pushing) জন্য ব্যবহার করা হয়। এছাড়াও ইমেজ ইন্সপেকশন, ইমেজ ট্রান্সফার 			    (Registry to Registry), ইমেজের তথ্য সংগ্রহ করার জন্য কাজ করে।     

Install Container tools with Podman:
------------------------------------

[root@node1 ~]# yum install container-tools -y

[root@node2 ~]# yum info container-tools

Check Podman Version:
---------------------
[root@node1 ~]# podman version
[root@node1 ~]# podman -v
[root@node1 ~]# podman help
[root@node1 ~]# podman info

Podman এর জন্য প্রয়োজনীয় কমান্ডঃ
---------------------------
[root@node1 ~]# podman build
[root@node1 ~]# podman run
[root@node1 ~]# podman pull
[root@node1 ~]# podman start
[root@node1 ~]# podman stop
[root@node1 ~]# podman ps
[root@node1 ~]# podman rmi
[root@node1 ~]# podman rm
[root@node1 ~]# podman restart
[root@node1 ~]# podman images
[root@node1 ~]# podman search 


* নিচের ফাইলটি এডিট করে রেজিস্ট্রি লোকেশন পরিবর্তন করা যাবেঃ
--------------------------------------------
* Registry Location: /etc/containers/registries.conf

 => Docker Hub
 => RedHat Registry
 => CentOS Registry
 => Fedora Registry 

registries:
  search:
  - registry.access.redhat.com
  - registry.redhat.io
  - docker.io

Listing Available Images:
-------------------------
নিচের কমান্ডের মাধ্যমে লোকাল সিস্টেমে কি কি ইমেজ নামানো (Pull) আছে সেটা জানা যাবে। 

[root@node1 ~]# podman images
REPOSITORY  TAG         IMAGE ID    CREATED     SIZE

রেজিস্ট্রিতে ইমেজ খোঁজার (Search) জন্য নিচের কমান্ডঃ 
---------------------------------------
[root@node1 ~]# podman search httpd
[root@node1 ~]# podman search nginx
[root@node1 ~]# podman search ubuntu
[root@node1 ~]# podman search centos 
[root@node1 ~]# podman search centos | grep 'centos7'

docker.io/publici/httpd                                                      httpd:latest
docker.io/manasip/httpd
docker.io/centos/httpd
docker.io/centos/httpd-24-centos7:latest

Image inspect to Docker Registry using skopeo:
----------------------------------------------
[root@node1 ~]# skopeo inspect docker://registry.access.redhat.com/rhel7
[root@node1 ~]# skopeo inspect docker://docker.io/library/ubuntu

Pull an Image to local:
-----------------------
[root@node1 ~]# podman pull docker.io/library/nginx 
[root@node1 ~]# podman pull docker.io/library/ubuntu 
[root@node1 ~]# podman pull docker.io/library/httpd  
[root@node1 ~]# podman pull docker.io/centos/httpd-24-centos7

Available pulled images:
------------------------
সকল লোকাল (Pulled) কন্টেইনার ইমেজ লিস্ট দেখার জন্য নিচের কমান্ডঃ 

[root@node1 ~]# podman images 

 REPOSITORY                         TAG         IMAGE ID      CREATED       SIZE
 docker.io/library/httpd            latest      1132a4fc88fa  9 days ago    148 MB
 docker.io/library/ubuntu           latest      ba6acccedd29  2 weeks ago   75.2 MB
 docker.io/library/nginx            latest      87a94228f133  2 weeks ago   138 MB
 docker.io/centos/httpd-24-centos7  latest      5ae4c76f6a3e  3 months ago  361 MB

Image ID ব্যবহার করে ইমেজ সম্পর্কে বিস্তারিত জানার জন্য নিচের কমান্ডঃ 
---------------------------------------------------
[root@node1 ~]# podman inspect <image id> 

লোকাল Image রিমুভ করার জন্য নিচের কমান্ডঃ
--------------------------------
[root@node1 ~]# podman rmi <image id> 
[root@node1 ~]# podman rmi -a

রানিং কন্টেইনারের (Container) সেই লিস্ট দেখার জন্য নিচের কমান্ডঃ  
------------------------------------------------
[root@node1 ~]# podman ps           (শুধুমাত্র রানিং কমান্ডের লিস্ট দেখা যাবে)
[root@node1 ~]# podman ps -a        (Running + Exited) 

লোকাল ইমেজ ব্যবহার করে কন্টেইনার রান করার জন্য নিচের কমান্ডঃ 
-----------------------------------------------------------
[root@node2 ~]# podman run -d <image id or image name> 
[root@node2 ~]# podman run -d httpd     (httpd কন্টেইনার ব্রাকগ্রাউন্ডে রান করবে)  
[root@node2 ~]# podman run -it ubuntu /bin/bash     (Ubuntu run in interactive Shell) 

-d: detached (Run container in background and print container ID)
-t: tty (Allocate a Pseudo-TTY for container) 
-i: interactive 
-p: --publish (define port number to the host)

root@0430d193aa3d:/# ls
root@0430d193aa3d:/# cat /etc/os-release
NAME="Ubuntu"

root@0430d193aa3d:/# uname -r
root@0430d193aa3d:/# cat /etc/resolv.conf
root@0430d193aa3d:/# free -m
root@0430d193aa3d:/# cat /proc/cpuinfo
root@0430d193aa3d:/# cat /etc/passwd
root@0430d193aa3d:/# top
root@0430d193aa3d:/# useradd robin
root@0430d193aa3d:/# passwd robin
root@0430d193aa3d:/# id robin
root@0430d193aa3d:/# cat /etc/passwd
root@0430d193aa3d:/# exit

Install packages:
-----------------
root@0430d193aa3d:/# apt-get -y update
root@0430d193aa3d:/# apt-get -y install vi
root@0430d193aa3d:/# apt-get -y install net-tools 

[root@node2 ~]# podman ps -a
CONTAINER ID  IMAGE                           COMMAND           CREATED         STATUS         PORTS       NAMES
302398173a6a  docker.io/library/httpd:latest  httpd-foreground  23 minutes ago  Exited (0) 10 seconds ago   

* কন্টেইনারের মধ্যে নতুন করে প্রসেস টেবিল (PID) তৈরি হবে, যেটা হোস্ট সিস্টেম থেকে দেখা যাবে না। 
* কন্টেইনার মধ্যে আলাদা নেটওয়ার্ক ইন্টারফেস তৈরি হবে এবং DHCP এর মাধ্যমে আইপি (Private) অ্যাড্রেস পাবে। 
* কন্টেইনার কর্তৃক নতুন ফাইল সিস্টেম তৈরি হবে। 

সরাসরি রেজিস্ট্রি থেকে ইমেজ রান করা যায়। এক্ষেত্রে ইমেজ লোকাল সিস্টমে সংরক্ষিত থাকে, এবং পরবর্তীতে কন্টেইনার রান করা যায়। 
----------------------------------------------------------------------------------------------------
[root@node1 ~]# podman run -d nginx         (এখানে '-d=detach' ব্যাকগ্রাউন্ডে রান করার জন্য + কন্টেইনার আইডি প্রিন্ট করবে) 
[root@node1 ~]# podman images
[root@node1 ~]# podman ps -a

ব্যাকগ্রাউন্ডে রানিং থাকা অবস্থায় কোনো কন্টেইনারের মধ্যে প্রবেশ/প্রসেস রান জন্য নিচের কমান্ডঃ 
-------------------------------------------------------------------------
 [root@node2 ~]# podman exec -it <Conatiner ID> /bin/bash
 [root@node2 ~]# podman exec -it 2e83a5cff7ea /bin/bash
  root@85b7824a0534:/usr/local/apache2#

কন্টেইনার Stop, Start এবং Remove করার জন্য নিচের কমান্ডঃ 
--------------------------------------------------
[root@node1 ~]# podman stop <container id> 
[root@node1 ~]# podman ps -a
[root@node1 ~]# podman start <container id>         (Exited Conatiner also Tested)
[root@node1 ~]# podman rm <container id> 

[root@node1 ~]# podman ps -a

নোটঃ সরাসরি ইমেজ থেকে কন্টেইনার স্টার্ট করা যাবে না। শুধু মাত্র স্টপ (Exited) কন্টেইনার থেকেই প্রসেস স্টার্ট করা যাবে। 
কন্টেইনার রিমুভ করার পুর্বে এটাকে স্টপ করে নিতে হবে। 

Project: Run a service inside a container (Builiding website using Apache httpd)
--------------------------------------------------------------------------------
[root@node1 ~]# mkdir /opt/web1
[root@node1 ~]# mkdir /opt/web2
[root@node1 ~]# mkdir /opt/web3

[root@node1 ~]# podman run -dt --name web1_info -p 8081:80 -v /opt/web1/:/usr/local/apache2/htdocs/ httpd
[root@node1 ~]# podman run -dt --name web2_net -p 8082:80 -v /opt/web2/:/usr/local/apache2/htdocs/ httpd
[root@node1 ~]# podman run -dt --name web3_com -p 8083:80 -v /opt/web3/:/usr/local/apache2/htdocs/ httpd
[root@node1 ~]# podman ps 

Create Sample WebPage:
----------------------
[root@node1 ~]# cd /opt/web1
[root@node1 web1]# vim index.html

	<html>
	<head>
	<body bgcolor="#f25dfd">
	<h1 allign="center"> ##### welcome to example.info  ### </h1>
	</body>
	</head>
	</html>

নোটঃ উপরের মত অন্য দুইটি ডিরেক্টরি 'web2 & web3' এর মধ্যে আলাদা আলাদা দুইটি 'index.html' ফাইল তৈরি করতে হবে। 

Try Test Pages:
---------------
http://172.25.11.254:8081
http://172.25.11.254:8082
http://172.25.11.254:8083

[root@node1 ~]# reboot
[root@node1 ~]# podman ps
[root@node1 ~]# podman ps -a
[root@node1 ~]# podman start <servie Name>

Start a container on boot with Podman and Systemd:
--------------------------------------------------
[root@node1 ~]# podman generate systemd --new --name web1_info 
[root@node1 ~]# podman generate systemd --new --name web1_info > /etc/systemd/system/web1.service

[root@node1 ~]# ls -al /etc/systemd/system/web1.service
-rw-r--r--. 1 root root 821 Nov 30 07:49 /etc/systemd/system/web1.service

[root@node1 ~]# systemctl start web1.service
[root@node1 ~]# systemctl enable web1.service
[root@node1 ~]# systemctl status web1.service

[root@node1 ~]# reboot 
[root@node1 ~]# podman ps

=================== Thank you =====================

অপারেটিং সিস্টেমের ইমেজ সমূহ, যেমনঃ ubuntu, centos7 রান করলে, ইমেজ 'exited' মোডে থাকবে। নিচের কমান্ডের মাধ্যেম 'ubuntu' ইমেজকে সরাসরি (রেজিস্ট্রি হতে) রান করে শেলে ঢোকা হয়েছেঃ 

RnD Section
**************
[root@node2 /]# cat /etc/os-release
[root@node2 /]#  yum install httpd -y
[root@node2 /]# cd /var/www/html/
[root@node2 /]# vi index.html

HEllo World !!!

[root@node2 /]# /usr/sbin/httpd


[root@node1 ~]# podman run -it --network host centos:7
--network - Connect a container to a network








