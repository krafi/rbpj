LVM Introduction:
-----------------
 যখন লিনাক্স (CentOS/Red Hat) ইন্সটল করা হয় তখন '/boot' বা '/boot/efi', '/' এবং 'Swap' এই তিনটা পার্টিশন করা হয়। উক্ত পার্টিশন সমূহ
 'Standard' পার্টিশন পদ্ধতি ব্যবহার করে তৈরি করা হয়েছিল। কিন্ত 'Standard' পার্টিশন পদ্ধতিতে পার্টিশন তৈরি করলে, ভবিষ্যতে পার্টিশনে কোনো স্পেস বাড়ানো বা কমানো যায় না। যেটা প্রডাকশন সার্ভারে বড় একটা সমস্যা হয়ে যায়। কারন, ভবিষ্যতে সার্ভারের স্টোরেজ ফুরিয়ে গেলে স্টোরেজ বাড়ানো দরকার হতে পারে। 

RHEL 9 Install Method:
----------------------
           1) Standard    (এই পদ্ধতিতে ভবিষ্যতে কোনো পার্টিশনের স্পেস বাড়ানো বা কমানো যাবে না)
           2) LVM Method  (ভবিষ্যতে ইচ্ছামত স্পেস বাড়ানো বা কমানো  যাবে। পাশাপাশি অতিরক্ত Hard disk অ্যাড  বা রিমুভ করাও যাবে) 

 /boot/efi - 200MiB
 /boot - 1024 MiB
 /     - 20 GiB
 Swap  - 2048 MiB

নোটঃ '/boot' বা '/boot/efi' পার্টিশন LVM হবে না। 

[root@localhost ~]# lsblk
[root@localhost ~]# fdisk -l 

এই ল্যাব ডেমোটি প্র্যাকটিস করার জন্য '/dev/sda5' '/dev/sda6' '/dev/sda7' নামে 250MB সাইজের তিনটি পার্টিশন তৈরি করতে হবে এবং সেগুলার পার্টিশন
 আইডি পরিবর্তন করে Linux থেকে Linux LVM করতে হবে। নিচে এই ব্যাপারে বিস্তারিত কমান্ড সহ দেখানো হয়েছেঃ 

[root@localhost ~]# fdisk /dev/sda  
 
Command (m for help): n

Selected partition 5
First sector (12314624-16777215, default 12314624): {Press Enter}
Using default value 12314624
Last sector, +sectors or +size{K,M,G} (12314624-16777215, default 16777215): {Press Enter}
Using default value 16777215
Partition 4 of type Extended 
Command (m for help): p

Command (m for help): n
First sector (902815744-976771071, default 902815744):  [press Enter]
Last ... +size{K,M,G} (902815744-976771071, default 976771071): +250M 
Command (m for help): p
Command (m for help): t
Command (m for help): Enter your patition no 
Command (m for help): l
Command (m for help): 8e (MBR), 31/30 (GPT)
Command (m for help): p
Command (m for help): w

Note: Do 2 more parttion for LVM 

[root@localhost ~]# lsblk

Physical Volume Create:
-----------------------
[root@localhost ~]# pvcreate /dev/sda5 
[root@localhost ~]# pvcreate /dev/sda6 
[root@localhost ~]# pvcreate /dev/sda7

[root@localhost ~]# pvdisplay 
[root@localhost ~]# pvscan

# yum install lvm2

Group Volume Create:
--------------------
সবগুলা ফিজিক্যাল ভলিউম (PV) মিলে একটা গ্রুপ ভলিউম (VG) তৈরি করা হবে। 

[root@localhost ~]# vgcreate vg1 /dev/sda5 /dev/sda6 /dev/sda7
[root@localhost ~]# vgdisplay 
[root@localhost ~]# vgscan 

ডিফল্ট ফিজিক্যাল এক্সটেন্ট (PE) সাইজ '4 MiB' এবং ভলিউম গ্রুপের সাইজ '744 MiB' অর্থাৎ এখানে মোট ১৮৬ টি  '4 MiB' ফিজিক্যাল এক্সটেন্ট (PE) হয়েছে। 

উল্লেখ্য যে, RHCSA ভেন্ডর এক্সামে ডিফল্ট ফিজিক্যাল এক্সটেন্ট (PE) সাইজ 4MiB পরিবর্তে '8MiB' করতে বলবে।

Note for Exam: vgcreate –s 8M vg1 /dev/vda5 /dev/vda6 /dev/vda7

Logical Volume Create:
----------------------
[root@localhost ~]# lvcreate -n lv1 -L 400M vg1
[root@localhost ~]# lvcreate -n lv2 -L 200M vg1

[root@localhost ~]# lvscan

[root@localhost ~]# lvdisplay

[root@localhost ~]# mkfs.xfs /dev/vg1/lv1 
[root@localhost ~]# mkdir /lvdata1
[root@localhost ~]# mount /dev/vg1/lv1 /lvdata1
[root@localhost ~]# df -HT 
[root@localhost ~]# 

[root@localhost ~]# mkfs.ext4 /dev/vg1/lv2 
[root@localhost ~]# mkdir /lvdata2
[root@localhost ~]# mount /dev/vg1/lv2 /lvdata2
[root@localhost ~]# df -HT 
[root@localhost ~]# 

Parmanent Mount:
----------------
 উপরের মাউন্টের পরে সিস্টেম রিবুট দিলে মাউন্ট পয়েন্ট চলে যাবে। এইজন্য স্থায়ী ভাবে মাউণ্ট করার জন্য নিচের '/etc/fstab' ফাইলে এন্ট্রি দিতে হবেঃ 

[root@localhost ~]# vim /etc/fstab
 :set nu

/dev/vg1/lv1	/lvdata1    xfs     defaults  0 0
/dev/vg1/lv2	/lvdata2    ext4    defaults  0 0

[root@localhost ~]# mount -a       ; ফাইল সিনট্যাক্স চেক করার জন্য। 
[root@localhost ~]# mount          ; মাউণ্ট হয়েছে কিনা দেখার জন্য। 
[root@localhost ~]# reboot

VG Extended:
------------
এই ল্যাবটি প্র্যাকটিস করার জন্য প্রথমে ভার্চুয়াল মেশিনে (Redhat/CentOS) -এ 5GiB সাইজের আরেকটি ডিস্ক অ্যাড করতে হবে। অ্যাড করার করার পরে নিচের কমান্ড দিয়ে দেখা যাবে, ডিস্কটি '/dev/sdb' নামে পেয়েছে। VG এক্সটেন্ড করার জন্য প্রথমে নতুন ডিস্কটি ফিজিক্যাল ভলিউমে (PV) তে কনভার্ট করতে হবে। 

[root@localhost ~]# fdisk -l 

=> fdisk /dev/sdb   (5GB) 

[root@localhost ~]# pvcreate /dev/sdb
[root@localhost ~]# pvscan

=> vgextend

[root@localhost ~]# vgextend vg1 /dev/sdb
[root@localhost ~]# vgdisplay

=> lvextend (xfs file system)

[root@localhost ~]# lvextend -L +100M /dev/vg1/lv1 
[root@localhost ~]# df -HT | grep lvdata
[root@localhost ~]# lvscan
[root@localhost ~]# xfs_growfs /lvdata1
[root@localhost ~]# df -HT | grep lvdata

=> lvextend (ext4 file system)

[root@localhost ~]# lvextend -L +100M /dev/vg1/lv2 
[root@localhost ~]# df -HT | grep lvdata2
[root@localhost ~]# lvscan
[root@localhost ~]# resize2fs /dev/vg1/lv2 
[root@localhost ~]# df -HT | grep lvdata

Lv remove:
----------
[root@localhost ~]# vim /etc/fstab           ;remove fstab entery
[root@localhost ~]# umount /lvdata1
[root@localhost ~]# umount /lvdata2

[root@localhost ~]# lsblk

[root@localhost ~]# lvremove /dev/vg1/lv1
[root@localhost ~]# lvremove /dev/vg1/lv2

VG Remove:
----------
[root@localhost ~]# vgremove vg1

PV Remove:
----------
[root@localhost ~]# pvremove /dev/vda5           
[root@localhost ~]# pvremove /dev/vda6           
[root@localhost ~]# pvremove /dev/vda7          
[root@localhost ~]# pvremove /dev/vda8           

[root@localhost ~]# fdisk /dev/vda
Command (m for help): d
Partition number (1-8, default 8): 4
Command (m for help): w

[root@localhost ~]# fdisk -l
 /dev/vda1
 /dev/vda2
 /dev/vda3

[root@localhost ~]# reboot

Working with Linux Swap Partition:
----------------------------------
 আমরা যখন লিনাক্স (CentOS/RHEL) ইন্সটল করি তখন তিনটি পার্টিশন তৈরি করি, '/boot' বা '/boot/efi', /, এবং 'swap' পার্টিশন। 
এর মধ্যে '/boot' এবং '/' হচ্ছে ডাটা বা সিস্টেম পার্টিশন, যেখানে ডাটা বা অপারেটিং সিস্টেমের বিভিন্ন কম্পোনেন্ট জমা থাকে। 
আর 'swap' পার্টিশন টা তৈরি করা হয়, ভার্চুয়াল মেমোরি বা সেকেন্ডারি মেমোরি হিসেবে। swap পার্টিশনে swap সিগনেচার থাকে বিধায় এটা মেমরির (RAM) 
মত ব্যবহার করা হয়। যখন CentOS/RedHat ইন্সটল করা হয়েছিল, তখন আমাদের প্রয়োজন অনুযায়ী 2048 MB (2 GiB) swap দিয়ে উল্লেখ করেছিলাম। 
এখন পরবর্তীতে যদি অতিরিক্ত swap স্পেস যোগ করার দরকার হয়, তাহলে আমরা নিচের পদ্ধতিতে যোগ করতে পারবো।
  
[root@serverX ~]# free -m    ; মেমোরি (RAM) এবং swap মেমোরির তথ্য পাওয়া যাবে। 

 Minimum required Swap Memory:
 ------------------------------
 RAM		Recommended Swap Space:
 --------------------------------------
 2GB or Less		Twice of RAM 
 2GB to 8GB		Equal to RAM 
 8GB to 256GB		Min 4GB 

Note: 4:1 

[root@serverX ~]# fdisk -l
/dev/sda2  1026048  5220351  4194304    2G Linux swap
 
[root@serverX ~]# lsblk

[root@serverX ~]# fdisk /dev/sda

Command (m for help): p
Command (m for help): n

Partition number (5-128, default 5): 5

First sector (12314624-16777215, default 12314624): {Press Enter}
Using default value 12314624
Last sector, +sectors or +size{K,M,G} (12314624-16777215, default 16777215): +1G
Using default value 16777215
Partition 4 of type Extended and of size 1 GiB is set

Command (m for help): p
Command (m for help): t
Partition number (1-5, default 5): 5
de (type L to list all codes): L
Hex code (type L to list all codes): 82 (MBR), 19 (GPT)

Changed type of partition 'Linux filesystem' to 'Linux swap'.

Command (m for help): p
Command (m for help): w

Check Current Swap Status:
--------------------------
[root@serverX ~]# swapon --show      ; পূর্বের swap পার্টিশন দেখার কমান্ড। 

[root@serverX ~]# mkswap /dev/sda5                   ; swap পার্টিশন ফরম্যাট করার কমান্ড। 
Setting up swapspace version 1, size = 511996 KiB
no label, UUID=7906ac34-4e2a-4b06-b57d-79dd6c66399a

[root@serverX ~]# swapon /dev/sda5
[root@serverX ~]# swapon --show

[root@serverX ~]# free -m
             total       used       free     shared    buffers     cached
Swap:         3072          0       1211

[root@serverX ~]# swapon -s 

Filename       Type            Size    Used    Priority
/dev/vda5      partition       716796  0       -1
/dev/vda3      partition       524284  0       -2

Permanent Mount:
----------------
[root@serverX ~]# lsblk -f

[root@serverX ~]# vim /etc/fstab 
 :set nu

#UUID=aea149e5-a8b4-fce23877468c swap defaults  0 0    ;comment with #'
/dev/sda5            swap        swap defaults  0 0

[root@serverX ~]# mount -a 
[root@serverX ~]# swapoff -a 
[root@serverX ~]# swapon -a
[root@serverX ~]# free -m 

or

[root@serverX ~]# reboot
[root@serverX ~]# free -m 

 ================= The End ==================

Home Work: -1
-------------
Install CentOS 9 Stream using following Parameters:

Storage Size: 30 GiB
Partition Type: GPT
Installation Type: LVM
Volume Group Name: rhcsa
EFI Partition - Standard (200MB)  - /boot/efi
MBR  Partition - Standard (1024MB) - /boot
root Partition (/) - 20GiB (LVM) 
Swap - 		   - 2GiB  (LVM)
Free Space 	   - 7 GB

Homework 2: (LVM Based OS)
--------------------------
1. Extend your '/' partition 1G from current size.
2. Extend your 'swap' partition 1G from current size
3. Extend your Volume group (rhcsa) with additional 5G Hdd



